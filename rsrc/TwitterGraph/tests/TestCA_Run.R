context("Test Graph Creation Basic")

rm(list = ls())
source('~/Config.R')
setwd(workingDir)
source("lib/TestFunctions.R")
RunNeo4jFile(neoTestLocation,"graph.db-CC-TOPIC-TEST")

# Create 2 Schedules & Run Analysis ------------------------------------------------------

InsertTestRun()
conn <- dbConnect(MySQL(), user = dbUser, password = dbPwd, host = dbHost, dbname = dbName)
res <- dbSendQuery(conn, "SELECT * FROM run_schedule WHERE completed = 0 ORDER BY id;")
schedule <- dbFetch(res)
dbClearResult(res)
dbDisconnect(conn)

dbHost <- "localhost"
dbName <- "twitter_sna_test"
dbPwd <- ""
dbUser <- "root"
conn <- dbConnect(MySQL(), user = dbUser, password = dbPwd, host = dbHost, dbname = dbName)

sqlUpdate <- "INSERT INTO `twitter_sna_test`.`run_schedule` (`id`, `start_date`, `period_length`, `degree_vals`, `giant_modes`, `graphs_for_detection`, `completed`, `process_graph`, `do_detection`, `stop_words`, `unique_name`, `publish_tableau`, `small_user_thold`, `low_reach_thold`, `recalc_pr`, `update_bot`, `filter_bots`, `filter_small_users`, `use_mentions`, `clear_communities`, `n_top`, `n_top_flocks`, `min_count`, `comm_size`, `steps`, `curr_time`, `tweet_corpus_internal`, `lda_remove_tags`, `lda_ngram_length`, `lda_tdm_word_min`, `lda_tdm_word_max`, `lda_method`, `lda_topics_qty`, `lda_terms_depth`, `lda_topics_depth`, `lda_likelihood_min`, `flock_life`, `prune_flocks`, `calc_comm_words`, `n_top_users`) VALUES (99999, '2016-06-02 00:00:00', '48', 1, 'strongPlus,weak', 'strongPlus,gInDeg1weak' , 0, 1, 1, 'world,scientist,scientists,climate,change,global,warming' , 1, 1, 10, 888, 1, 1, 1, 1, 1, 1, 15, 20, 5, 5, 3, 1, 0, 1, 1, 3, 40, 'VEM', 10, 6, 3, 0.15, 8, 1, 0, 21);"
res <- dbSendQuery(conn, sqlUpdate)
dbClearResult(res)
res <- dbSendQuery(conn, "SELECT * FROM run_schedule WHERE completed = 0 ORDER BY id;")
schedule <- dbFetch(res)
dbClearResult(res)
dbDisconnect(conn)

source("src/RunAnalysis.R")

# Tear Down & Close ------------------------------------------------------
conn <- dbConnect(MySQL(), user = dbUser, password = dbPwd, host = dbHost, dbname = dbName)
sqlTearDown <- "DELETE FROM run_schedule WHERE id = 99999;"
res <- dbSendQuery(conn, sqlTearDown)
dbClearResult(res)
sqlTearDown <- "DELETE FROM run_schedule WHERE id = 9999;"
res <- dbSendQuery(conn, sqlTearDown)
dbClearResult(res)
dbDisconnect(conn)

test_that("Schedule Created", {
      
      expect_equal(scheduleList$scheduleItems[1],2)
      expect_equal(scheduleList$scheduleIdList[1],9999)
      expect_equal(scheduleList$scheduleIdList[2],99999)
      expect_equal(scheduleList$startDates[1],"2016 Jun 02 00")
      expect_equal(scheduleList$startDates[2],"2016 Jun 02 00")
      expect_equal(scheduleList$startTimeUnix[1],DateTimeUnix(dateString = "2016 Jun 02 00"))
      expect_equal(scheduleList$startTimeUnix[2],DateTimeUnix(dateString = "2016 Jun 02 00"))
      expect_equal(scheduleList$endDates[1],"2016 Jun 03 23")
      expect_equal(scheduleList$endDates[2],"2016 Jun 03 23")
      expect_equal(scheduleList$endTimeUnix[1],DateTimeUnix(dateString = "2016 Jun 03 23"))
      expect_equal(scheduleList$endTimeUnix[2],DateTimeUnix(dateString = "2016 Jun 03 23"))
      
      expect_equal(scheduleList$degreeValsList[1],"1")
      expect_equal(scheduleList$degreeValsList[2],"1")
      expect_equal(scheduleList$giantModesList[1],"strongPlus,weak")
      expect_equal(scheduleList$giantModesList[2],"strongPlus,weak")
      expect_equal(scheduleList$graphsForDetectionList[1],"strongPlus,gInDeg1weak")
      expect_equal(scheduleList$graphsForDetectionList[2],"strongPlus,gInDeg1weak")
      
      expect_equal(scheduleList$processGraphList[1],TRUE)
      expect_equal(scheduleList$processGraphList[2],TRUE)
      expect_equal(scheduleList$doDetectionList[1],TRUE)
      expect_equal(scheduleList$doDetectionList[2],TRUE)
      expect_equal(scheduleList$publishTableauList[1],TRUE)
      expect_equal(scheduleList$publishTableauList[2],TRUE)
      expect_equal(scheduleList$clearCommunitiesList[1],TRUE)
      expect_equal(scheduleList$clearCommunitiesList[2],TRUE)
      expect_equal(scheduleList$recalcPRList[1],TRUE)
      expect_equal(scheduleList$recalcPRList[2],TRUE)
      expect_equal(scheduleList$updateBotList[1],TRUE)
      expect_equal(scheduleList$updateBotList[2],TRUE)
      expect_equal(scheduleList$filterBotsList[1],TRUE)
      expect_equal(scheduleList$filterBotsList[2],TRUE)
      
      expect_equal(scheduleList$filterSmallUsersList[1],TRUE)
      expect_equal(scheduleList$filterSmallUsersList[2],TRUE)
      expect_equal(scheduleList$smallUserTHoldList[1],100)
      expect_equal(scheduleList$smallUserTHoldList[2],10)
      expect_equal(scheduleList$lowReachTHoldList[1],10000)
      expect_equal(scheduleList$lowReachTHoldList[2],888)
      expect_equal(scheduleList$useMentionsList[1],TRUE)
      expect_equal(scheduleList$useMentionsList[2],TRUE)
      expect_equal(scheduleList$minCountList[1],5)
      expect_equal(scheduleList$minCountList[2],5)
      expect_equal(scheduleList$nTopList[1],100)
      expect_equal(scheduleList$nTopList[2],15)
      expect_equal(scheduleList$nTopFlocksList[1],20)
      expect_equal(scheduleList$nTopFlocksList[2],20)
      
      expect_equal(scheduleList$commSizeList[1],5)
      expect_equal(scheduleList$commSizeList[2],5)
      expect_equal(scheduleList$stepsList[1],3)
      expect_equal(scheduleList$stepsList[2],3)
      expect_equal(scheduleList$currTimeList[1],TRUE)
      expect_equal(scheduleList$currTimeList[2],TRUE)
      #expect_equal(scheduleList$stopWordsList[1],"vote,voting,june,day,days,htt,cameron,david,people,poll,membership,election,pledge,eu,referendum,brexit,vote,british,uk,exit,exiting,countries,european,campaign,big,hasnt,weve,fight,leave,stay,deal,dont,day,remain,britain,europe,britains,eur,leaving,country")
      #expect_equal(scheduleList$stopWordsList[2],"vote,british,uk,exit,exiting,countries,european,campaign,big,hasnt,weve,fight,leave,stay,deal,dont,day,remain,britain,europe,britains,eur,leaving,country")
      expect_equal(scheduleList$stopWordsList[1],"world,scientist,scientists,climate,change,global,warming")
      expect_equal(scheduleList$stopWordsList[2],"world,scientist,scientists,climate,change,global,warming")
      expect_equal(scheduleList$uniqueNameList[1],TRUE)
      expect_equal(scheduleList$uniqueNameList[2],TRUE)
      expect_equal(scheduleList$tweetCorpusInternalList[1],FALSE)
      expect_equal(scheduleList$tweetCorpusInternalList[2],FALSE)
      expect_equal(scheduleList$removeTagsTAList[1],1)
      expect_equal(scheduleList$removeTagsTAList[2],1)
      expect_equal(scheduleList$nGramLengthList[1],1)
      expect_equal(scheduleList$nGramLengthList[2],1)
      expect_equal(scheduleList$tdmWordMinList[1],3)
      expect_equal(scheduleList$tdmWordMinList[2],3)
      expect_equal(scheduleList$tdmWordMaxList[1],40)
      expect_equal(scheduleList$tdmWordMaxList[2],40)
      
      expect_equal(scheduleList$ldaMethodList[1],"VEM")
      expect_equal(scheduleList$ldaMethodList[2],"VEM")
      expect_equal(scheduleList$topicsQtyList[1],10)
      expect_equal(scheduleList$topicsQtyList[2],10)
      expect_equal(scheduleList$termsDepthList[1],6)
      expect_equal(scheduleList$termsDepthList[2],6)
      expect_equal(scheduleList$topicsDepthList[1],3)
      expect_equal(scheduleList$topicsDepthList[2],3)
      expect_equal(scheduleList$likelihoodMinList[1],0.15)
      expect_equal(scheduleList$likelihoodMinList[2],0.15)
      expect_equal(scheduleList$pruneFlocksList[1],TRUE)
      expect_equal(scheduleList$pruneFlocksList[2],TRUE)
      expect_equal(scheduleList$flockLifeList[1],8)
      expect_equal(scheduleList$flockLifeList[2],8)
      expect_equal(scheduleList$calcCommWordsList[1],FALSE)
      expect_equal(scheduleList$calcCommWordsList[2],FALSE)
      expect_equal(scheduleList$nTopUsersList[1],20)
      expect_equal(scheduleList$nTopUsersList[2],21)
      
})

test_that("Basic Graph Stats", {
      
      # Neo4j Version ------------------------------------------------------
      expect_equal(graph$version, "3.0.1")
      
      # Basic Stats ------------------------------------------------------
      
      queryString <- QueryString(qsTweetsInPeriod)
      tweets <- cypher(graph, queryString,startTimeUnix = 0, endTimeUnix = 999999999999999)
      
      queryString <- QueryString(qsCountUsersInPeriod)
      usersCount <- na.omit(cypher(graph, queryString,startTimeUnix = 0, endTimeUnix = 999999999999999))
      
      tweets$date <- as.POSIXlt.numeric(tweets$t.unixtime,origin = "1970-01-01")
      tweets$day <- as.character(floor_date(tweets[,4] , "day" ) )
      usersCount <- select(tweets,-date) %>% group_by(.,day) %>% summarise(.,count = n())
      
      expect_equal(nrow(tweets),37820)
      expect_equal(usersCount$count[91],15)
      expect_equal(usersCount$day[1],"2010-01-27")
      expect_equal(usersCount$day[12],"2015-01-14")
      expect_equal(usersCount$count[11],1)
      expect_equal(usersCount$count[31],2)
      expect_equal(sum(usersCount$count),37820)
      
})


test_that("Basic Graph Processing", {
      
      # Nodes & Edges ------------------------------------------------------
      expect_equal(length(V(g)), 3642)
      expect_equal(length(E(g)), 4040)
      
      # Data Frame of Users
      userTable <- as_data_frame(g, what = c("Vertices"))
      
      expect_equal(sum(userTable$followers,na.rm = TRUE),810312033)
      
      
})

test_that("Output Graph", {
      fileName <- 'EU-TEST2016Jun02002016Jun0323_retweets_9999gInDeg1weak.graphml'
      retweet_graph = read.graph(paste0(graphDir,"/",fileName),format = c("graphml"))
      checkRankAttribute <- "rank" %in% names(vertex.attributes(retweet_graph))
      expect_true(checkRankAttribute)
      
      
})


test_that("Output SQL", {
      conn <- dbConnect(MySQL(), user = "root", password = '', host = 'localhost', dbname = 'twitter_sna_test')
      res <- dbSendQuery(conn = conn,statement = "SELECT * FROM flock_list;")
      flockTableMS <- dbFetch(res)
      dbClearResult(res)
      dbDisconnect(conn)
      
      # Test Rows Added to Flocks ------------------------------------------------------
      expect_equal(flockTableMS[1,13],"en")
      expect_equal(length(flockTableMS$Flock),21)
      expect_equal(flockTableMS$Flock[1],"BernieSanders")
      expect_equal(round(flockTableMS$`Page Rank`[1]),197)
      expect_equal(flockTableMS$`Mean Page Rank`[1], 9.39)
      expect_equal(flockTableMS$Reach[3], 342569)
      expect_equal(flockTableMS$`Mean Reach`[3], 31143)
      expect_equal(round(flockTableMS$Interesting[1]), 215)
      
      conn <- dbConnect(MySQL(), user = "root", password = '', host = 'localhost', dbname = 'twitter_sna_test')
      res <- dbSendQuery(conn = conn,statement = "SELECT * FROM topic_flock;")
      flockTableMS <- dbFetch(res)
      dbClearResult(res)
      dbDisconnect(conn)
      
      # Test Flock Terms ------------------------------------------------------
      expect_equal(flockTableMS[1,2],"BernieSanders")
      expect_gt(nrow(flockTableMS),20)
      expect_lt(nrow(flockTableMS),25)
      expect_match(paste(flockTableMS$Terms,collapse = " "),"(hoax|trump|warns|planet)")
      expect_match(paste(flockTableMS$Terms,collapse = " "),"(june|future|reef|mcmurray)")
      
      conn <- dbConnect(MySQL(), user = "root", password = '', host = 'localhost', dbname = 'twitter_sna_test')
      res <- dbSendQuery(conn = conn,statement = "SELECT * FROM flock_user;")
      flockTableMS <- dbFetch(res)
      dbClearResult(res)
      dbDisconnect(conn)
      
      # Test Flock Terms ------------------------------------------------------
      expect_equal(nrow(flockTableMS),281)
      
      expect_equal(flockTableMS[105,3],"Seano")
      expect_equal(flockTableMS$`Re Tweets`[105],21)
      expect_equal(flockTableMS$`Reach`[109],34621)
      expect_equal(flockTableMS$`Reach RT`[105],57686)
      expect_equal(flockTableMS$`Reach Mentions`[109],34621)
      expect_equal(flockTableMS[1,3],"Bernie Sanders")
      expect_equal(flockTableMS$`Re Tweets`[1],854)
      expect_equal(flockTableMS$`Reach`[1],27320537)
      expect_equal(flockTableMS$`Reach RT`[1],25636919)
      expect_equal(flockTableMS$`Reach Mentions`[1],1683618)
})
print("Finished Tests")