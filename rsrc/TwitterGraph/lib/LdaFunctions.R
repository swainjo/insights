CreateTerms <- function(commText, communities, termsDepth, topicsDepth, topicsQty, ldaMethod, stopWords, tdmWordMin, tdmWordMax, nGramLength,removeTagsTA = TRUE){
      
      # Create Corpus from Tweet text  ------------------------------------
      corpusFrame <- commText %>% 
            group_by_(~Community) %>%
            summarize_(alpha_text = ~paste(alpha_text, collapse = ' ')) %>%
            arrange(.,Community)
      
      # Remove comms with no text from grouping
      commList <- corpusFrame$Community
      communitiesClean <- communities %>% filter(.,name %in% commList) %>%
            arrange(.,name)
      
      # Recreate id's for filtered list.
      communitiesClean$id <- 1:nrow(communitiesClean)
      
      corpusRaw <- VCorpus(DataframeSource(as.data.frame(corpusFrame$alpha_text)))
      
      # Clean Text ------------------------------------
      corpusClean <- CleanText(corpusRaw,stopWords = stopWords,removeTagsTA = removeTagsTA)
      
      # Create Document Term Matrix ------------------------------------
      print(paste0(Sys.time()," - Starting - Get DTM"))
      options(mc.cores = 1) # Workaroung bug - Sets the number of threads to use
      dtm <- DocumentTermMatrix(corpusClean, 
                                control = list(tokenize = function(x){RWeka::NGramTokenizer(x, RWeka::Weka_control(min = 1, max = nGramLength)) },
                                               wordLengths = c(tdmWordMin, tdmWordMax)))
      numcores <- detectCores()
      options(mc.cores = numcores) # Workaroung bug - ReSets the default number of threads to use
      print(paste0(Sys.time()," - Completed - Get DTM"))
      
      # Remove All Docs with no Text
      zeroRowsBool <- (row_sums(dtm) > 0)
      communitiesClean <- filter(communitiesClean, name %in% communitiesClean$name[zeroRowsBool])
      communitiesClean$id <- 1:nrow(communitiesClean)
      dtm <- dtm[row_sums(dtm) > 0,]
      
      # Run LDA ------------------------------------
      lda <- LDA(dtm, topicsQty, method = ldaMethod)
      
      # Generate Terms Lists  ------------------------------------
      termsList <- as.data.frame(terms(lda,termsDepth))
      topicsList <- as.data.frame(topics(lda,topicsDepth))
      commTerms <- CreateCommTerms(topicsList,termsList,
                                   communitiesClean[communitiesClean$id %in% Docs(dtm),],ldaGamma = lda@gamma)
      
      termsFrames <- list()
      termsFrames$termsList <- termsList
      termsFrames$topicsList <- topicsList
      termsFrames$commTerms <- commTerms
      
      return(termsFrames)
}


NgramTokenizer <- function(x) {
      # Split corpus by nGram - used by DocumentTermMatrix function
      RWeka::NGramTokenizer(x, RWeka::Weka_control(min = 1, max = nGramLength))
}

CleanText <- function(corpusRaw,stopWords,removeTagsTA = TRUE) {
      # Content Transformers to Match pattern and replace with "" or " "
      (ReplaceNothing <- content_transformer(function(x, pattern) gsub(pattern, "", x)))
      (ReplaceSpace <- content_transformer(function(x, pattern) gsub(pattern, " ", x)))
      
      # Transformations to clean the Corpus Text
      corpusRaw <- tm_map(corpusRaw, content_transformer(tolower))
      corpusRaw <- tm_map(corpusRaw, ReplaceSpace, "http\\S+\\s*") # Remove urls
      corpusRaw <- tm_map(corpusRaw, ReplaceSpace, "&amp_") # Remove Amps
      corpusRaw <- tm_map(corpusRaw, ReplaceSpace, " rt ") # Remove retweets
      corpusRaw <- tm_map(corpusRaw, ReplaceSpace, "@([A-Za-z0-9_]+)") # Remove users
      if (removeTagsTA == TRUE) {
            corpusRaw <- tm_map(corpusRaw, ReplaceSpace, "#([A-Za-z0-9_]+)") # Remove tags
      }
      corpusRaw <- tm_map(corpusRaw, ReplaceSpace, "[ |\t]{2,}") #Tabs
      corpusRaw <- tm_map(corpusRaw, removePunctuation)
      corpusRaw <- tm_map(corpusRaw, removeNumbers)
      corpusRaw <- tm_map(corpusRaw, function(x) removeWords(x, stopWords),lazy = TRUE)
      corpusRaw <- tm_map(corpusRaw, ReplaceSpace, "\n")
      corpusRaw <- tm_map(corpusRaw, stripWhitespace)
      
      return(corpusRaw)
}


TransposeTopicTabs <- function(listDF, colNames, 
                                melt = TRUE) {
      # Transpose TermsList output from LDA to Long Format
      listDF <- as.data.frame(t(listDF))
      colsTab <- ncol(listDF)
      colnames(listDF) <- (1:colsTab)
      listDF$id <- (1:nrow(listDF))
      listDF <- listDF[c(colsTab + 1,(1:colsTab))]
      
      if (melt == TRUE) {
            listDF <- listDF %>% melt(id = "id", value.factor = FALSE)
            colnames(listDF) <- (colNames)
      }
      i <- sapply(listDF, is.factor)
      listDF[i] <- lapply(listDF[i], as.character)
      return(listDF)
}

CreateCommTerms <- function(topicsList,termsList,topCommunities,ldaGamma) {
      # Get gamma values from LDA
      ldaGammaList <- as.data.frame(ldaGamma) %>%
            mutate(.,commId = row.names(.)) %>%
            gather(.,"commId")  
      
      names(ldaGammaList)[2] <- "topicId" 
      
      ldaGammaList <- mutate(ldaGammaList, topicId = str_sub(topicId,start = 2)) %>%
            mutate(.,uId = paste0(topicId,"-",commId))
      
      # Join topcs to Documents
      topicsList <- TransposeTopicTabs(topicsList,colNames = c("id","topicRank","topic")) %>%
            inner_join(topCommunities,.,by = c("id" = "id"))  %>%
            mutate(.,uId = paste0(topic,"-",id)) %>%
            inner_join(.,ldaGammaList,by = c("uId" = "uId")) %>%
            select(.,id,subGraph,name,topicRank,topic,likelihood = value)
      commTerms <- TransposeTopicTabs(termsList,colNames = c("id","termRank","term")) %>%
            inner_join(.,topicsList,by = c("id" = "topic")) %>%
            select(.,topicId = id,subGraph,name,topicRank,term,likelihood)
      return(commTerms)
      
}


