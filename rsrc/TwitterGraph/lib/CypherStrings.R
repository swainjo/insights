# Index List ----------------------------------------------------------
indexList <- list(list("Place","name"),
                  list("Tweet","alpha_text"),
                  list("Tweet","unixtime"),
                  list("Tweet","lang"),
                  list("Tweeted","screen_name"),
                  list("Tweeted","id"),
                  list("User","screen_name"),
                  list("Bot","screen_name"),
                  list("Bot","id"),
                  list("Community","mayor"),
                  list("Community","name")
)

constraintList <- list(list("TwitterList","name"),
                       list("Tweet","id"),
                       list("List","name"),
                       list("Network","name"),
                       list("Place","id"),
                       list("Topic","name"),
                       list("Community","u_name"),
                       list("Source","name"),
                       list("Link","url"),
                       list("CommunityGroup","name"),
                       list("Organisation","name"),
                       list("User","id"),
                       list("Hashtag","name"),
                       list("Collector","id"),
                       list("SearchTerm","id"),
                       list("Topic","name")
)


### Search Terms ---------------------------------------------------------------

# Examples taken from references to countries in Ebola Crisis.
kSearchLiberia <- '.*[lL]iberia.*'
kSearchAffectedCountries <- '.*[lL]iberia.*|.*[gG]uinea*|.*[sS]iera [lL]eone|.*[nN]igeria.*|.*[sS]enegal.*'

# kSearchAll ----------------------------------------------------------
kSearchAll <- '.*'

### Query Strings ----------------------------------------------------------

### Top Topics ----------------------------------------------------------
qsTopicsTop <- "MATCH (i:Influence)
WHERE TOINT(i.unixtime) <= TOINT({endTimeUnix})
WITH MAX(TOINT(i.unixtime)) AS maxTime
MATCH (i:Influence)-[r:IN_TOPIC]->(t:Topic) 
WHERE TOINT(i.unixtime) = maxTime
RETURN SUM(TOINT(i.score)) AS score, t.name AS topic
ORDER BY score DESC,topic
LIMIT {nTopics}"

### Top Topics Later----------------------------------------------------------
qsTopicsTopLater <- "MATCH (i:Influence)
WITH MIN(TOINT(i.unixtime)) AS minTime
MATCH (i:Influence)-[r:IN_TOPIC]->(t:Topic) 
WHERE TOINT(i.unixtime) = minTime
RETURN SUM(TOINT(i.score)) AS score, t.name AS topic
ORDER BY score DESC,topic
LIMIT {nTopics}"

### Topic Scores ----------------------------------------------------------
qsTopicsScores <- "MATCH (i:Influence)
WHERE TOINT(i.unixtime) <= TOINT({endTimeUnix})
WITH MAX(TOINT(i.unixtime)) AS maxTime
MATCH (u:User)-[h:HAS_INFLUENCE]->(i:Influence)-[r:IN_TOPIC]->(t:Topic) 
WHERE t.name IN ({topicList})
AND u.screen_name IN ({userList})
AND TOINT(i.unixtime) = maxTime
RETURN u.screen_name AS user,i.score AS score, t.name AS topic
ORDER BY score DESC,user"

### Topic Scores Later----------------------------------------------------------
qsTopicsScoresLater <- "MATCH (i:Influence)
WITH MIN(TOINT(i.unixtime)) AS minTime
MATCH (u:User)-[h:HAS_INFLUENCE]->(i:Influence)-[r:IN_TOPIC]->(t:Topic) 
WHERE t.name  IN ({topicList})
AND u.screen_name IN ({userList})
AND TOINT(i.unixtime) = minTime
RETURN u.screen_name AS user,i.score AS score, t.name AS topic
ORDER BY score DESC,user"

### Tweets In Period ----------------------------------------------------------
qsTweetsInPeriod <- "MATCH (t:Tweet)
WHERE t.unixtime >= TOINT({startTimeUnix}) AND t.unixtime <= TOINT({endTimeUnix})
RETURN t.unixtime,t.id,t.lang"

### Brokerage by User ----------------------------------------------------------
qsBrokeragebyUser <- "MATCH (n:Network)<-[i:IN_NETWORK]-(u:User) 
WHERE u.id IN {userIds}
AND n.name = {networkName}
RETURN u.id AS id, i.brokerage AS brokerage"

# Count Tweets ----------------------------------------------------------
qsCountTweets <- "MATCH (t1:Tweet) 
WHERE t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
RETURN COUNT(*)"

# Count Users ----------------------------------------------------------
qsCountUsersInPeriod <- "MATCH (u:User)-[:POSTS]->(t1:Tweet) 
WHERE t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
RETURN COUNT(DISTINCT u) AS UserCount"

# Create Flocks -----------------------------------------------------
qsCreateFlocks <- "
MATCH (m:User)-[:MAYOR_OF]->(cm:Community)
WHERE NOT(EXISTS(cm.flock_updated)) 
SET cm.flock_updated = TRUE
WITH DISTINCT m.screen_name AS screen_name, COLLECT(cm.name) AS cm_list
MERGE (cm1:Community {name:screen_name, type:'flock'})
ON CREATE SET cm1.iteration = 1
ON MATCH SET cm1.iteration = 
CASE WHEN NOT(EXISTS(cm1.iteration)) THEN 1
ELSE cm1.iteration + 1 
END
WITH cm1,cm_list
MATCH (cm2:Community)<-[:MEMBER_OF_COMMUNITY]-(u:User)
WHERE cm2.name IN(cm_list)
WITH cm1, u, COUNT(u) AS Weight
MERGE (u)-[me:MEMBER_OF_COMMUNITY]->(cm1)
SET
me.weight = Weight,
me.active = TRUE,
me.last_iteration = cm1.iteration"

# Create & Prune Flocks -----------------------------------------------------
qsPruneFlocks <- "
WITH cm1
MATCH (cm1)<-[me1:MEMBER_OF_COMMUNITY]-(u2)
WHERE me1.last_iteration < cm1.iteration-{flockLife}
DELETE me1"

# Create & Prune Flocks -----------------------------------------------------
qsCreatePruneFlocks <- paste0(qsCreateFlocks, qsPruneFlocks) 

# Trim Flocks -----------------------------------------------------
qsTrimFlocks <- "
MATCH (cm1:Community {type:'flock'})
WITH cm1
MATCH (cm1)<-[m1:MEMBER_OF_COMMUNITY]-(u:User)
WHERE u.screen_name = cm1.name
WITH cm1, MAX(m1.weight) AS max_weight
MATCH (cm1)<-[m2:MEMBER_OF_COMMUNITY]-(u:User)
WHERE (m2.weight <= CEIL((max_weight*.05))) AND (m2.weight < FLOOR(max_weight*0.5))
SET m2.active = FALSE"

# Initialise Flocks -----------------------------------------------------
qsInitialiseFlocks <- "
MATCH (cm1:Community {type:'flock'})<-[m1:MEMBER_OF_COMMUNITY]-(u:User)
WHERE NOT EXISTS(m1.active)
SET m1.active = TRUE"

# Update Bot Labels ------------------------------------------------------------
qsUpdateBotLabels <- "MATCH (t:Tweet)<-[:POSTS]-(u2:User)
WHERE t.unixtime >= TOINT({startTimeUnix}) AND t.unixtime <= TOINT({endTimeUnix})
AND NOT u2:Bot
WITH COUNT(t) AS totalTweets, u2
WHERE totalTweets > 10
MATCH (u1:User)<-[m:MENTIONS]-(t2:Tweet)<-[:POSTS]-(u2)
WHERE t2.unixtime >= TOINT({startTimeUnix}) AND t2.unixtime <= TOINT({endTimeUnix})
WITH t2,u2,totalTweets, COUNT(m) AS mentionCount 
WITH u2 AS User, totalTweets,AVG(mentionCount) AS avgMentions
WHERE avgMentions > 4
SET User :Bot"

# Update Topic Labels ----------------------------------------------------------
qsUpdateTopicLabels <- "MATCH (t:Tweet)
WHERE t.unixtime >= TOINT({startTimeUnix}) AND t.unixtime <= TOINT({endTimeUnix})
WITH t
MATCH (tp1:Topic)
WITH tp1,t
WHERE t.text =~ tp1.qstring
WITH t,tp1
MERGE (t)-[r:ABOUT_TOPIC]->(tp1)"

# Update Not Giant Component ----------------------------------------------------------
qsUpdateNotGiant <- "UNWIND {notGiantList} AS t
WITH t
MATCH (cm:Community {name:'Not Giant - CD'}),(u:User)
WHERE u.id = TOINT(t)
WITH cm,u MERGE (u)-[r:MEMBER_OF_COMMUNITY {status: 'auto'}]->(cm)"

# Get Community by Name ----------------------------------------------------------
qsGetCommunity <- "MATCH (cm:Community {name:{comName}}) RETURN cm"

# Get Community Membership ----------------------------------------------------------
qsGetCommunityMembership <- "MATCH (u:User)-[MEMBER_OF_COMMUNITY]->(c:Community) RETURN u.screen_name, c.name, c.subgraph"

# Get Flock Membership ----------------------------------------------------------
qsGetFlockMembership <- "MATCH (c:Community)<-[mc:MEMBER_OF_COMMUNITY]-(u:User)
WHERE c.type = 'flock' AND mc.active = TRUE
RETURN
u.screen_name,u.name, u.location, u.description, u.profile_image_url, mc.weight, c.name"

# Get Flock Membership List ----------------------------------------------------------
qsGetFlockMembershipList <- "MATCH (c:Community)<-[mc:MEMBER_OF_COMMUNITY]-(u:User)
WHERE c.type = 'flock' AND c.name = {flock} AND 
mc.active = TRUE
RETURN
COLLECT(u.screen_name), COUNT(u)"

# Get Users ----------------------------------------------------------
qsGetUser <- "MATCH (u:User {id:TOINT({uId})}) RETURN u"

# Communities clear ------------------------------------------------------
qsClearCommunitiesAll <- "MATCH (c:Community)-[n]-()
DELETE n,c"

# Communites by CD -------------------------------------------------------------
qsGetCommunitiesByCD <- "MATCH (cm:Community) 
WHERE cm.algorithm = {algorithm} AND
cm.subgraph = {subgraph}
RETURN cm.name"

# Communites by Display -------------------------------------------------------------
qsGetDisplayCommunities <- "MATCH (cm:Community-[:MEMBER_OF_GROUP]->(cg:CommunityGroup {name:'Display'})
RETURN cm.name"

# Users by Hashtag -------------------------------------------------------------
# Get list of users for specific Hashtag
qGetUsersTextOrTag <- "MATCH (u1:User)-[:POSTS]->(t:Tweet)<-[:TAGS]-(h:Hashtag)
WHERE h.name =~ {search_text} OR t.text =~ {search_text}
RETURN DISTINCT u1.screen_name as screen_name u1.id AS id"

# Get list of users who retweeted a Post with a specific Hashtag 
qGetUsersTextOrTagRT <- "MATCH (u1:User)-[:POSTS]->(t1:Tweet)<-[:TAGS]-(h:Hashtag)
WHERE h.name =~ {search_text} OR t1.text =~ {search_text}
WITH t1
MATCH (u2:User)-[:POSTS]->(t2:Tweet)-[:RETWEETS]->(t1)
RETURN DISTINCT u2.screen_name as screen_name, u1.id AS id"

# JSON Error Fixes -------------------------------------------------------------
# Remove Backslash from Text
qReplaceBackSlash <- "MATCH (t:Tweet)
WHERE HAS (t.text) AND t.text =~ '.*\\\\.*'
SET t.text = REPLACE(t.text,'\\','/')"

# Remove backslash from name
qsSetSafeName <- "MATCH (u:User)
WHERE HAS (u.name) AND u.name =~ '.*[^a-zA-Z1-9\\d\\s :_\\p{P}\\p{L}\\p{M}\\p{S}].*'
SET u.original_name = u.name, u.name = 'safe name'"

# Remove non Chars from name
qsSetSafeName <- "MATCH (u:User)
WHERE HAS (u.name) AND u.name =~ '.*\\\\.*'
SET u.original_name = u.name, u.name = 'safe name'"

# Tweets by Users -------------------------------------------------------------
qsTweetsFromUsers <- "MATCH (u1:User)
WHERE u1.screen_name IN {names}
WITH u1
MATCH (u1)-[:POSTS]->(t1:Tweet)
RETURN t1.text"

# Fetch Community Tweets ----------------------------------------------------------
qsFetchCommunityTweets <- "MATCH (t1:Tweet)<-[p1:POSTS]-(u1:User)-[m:MEMBER_OF_COMMUNITY]->(cm:Community) 
WHERE cm.name = {community}
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
AND EXISTS(t1.text)
AND m.active <> FALSE
WITH u1,t1
OPTIONAL MATCH (t1)<-[r1:RETWEETS]-(t2:Tweet)<-[p2:POSTS]-(u2:User)
WHERE t2.unixtime >= TOINT({startTimeUnix}) AND t2.unixtime <= TOINT({endTimeUnix})
WITH t1,u1,(u1.followers) + SUM(u2.followers) as RTReach, COUNT(r1) AS RTCount
WHERE RTReach > 1000
RETURN u1.screen_name AS Name, toString(t1.id) AS Id, t1.text AS Tweet, RTCount, RTReach
ORDER BY RTReach DESC"

# Fetch Users Tweets ----------------------------------------------------------
qsFetchUsersTweetsLimit <- "MATCH (t1:Tweet)<-[p1:POSTS]-(u1:User) 
WHERE t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
AND u1.screen_name IN {userIds}
AND EXISTS(t1.text)
WITH u1,t1
OPTIONAL MATCH (t1)<-[r1:RETWEETS]-(t2:Tweet)<-[p2:POSTS]-(u2:User)
WHERE t2.unixtime >= TOINT({startTimeUnix}) AND t2.unixtime <= TOINT({endTimeUnix})
WITH t1,u1,(u1.followers) + SUM(u2.followers) as RTReach, COUNT(r1) AS RTCount
RETURN u1.screen_name AS Name, toString(t1.id) AS Id, t1.alpha_text AS Tweet, RTCount, RTReach
ORDER BY RTCount DESC, RTReach DESC
LIMIT {topN}"

# Get list of Tags and overall Counts
qsOverallTagCount <- "MATCH (t1:Tweet)<-[TAGS]-(h:Hashtag)
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
WITH  h, COUNT(h) AS TagCount
WHERE TagCount > {minCount}
RETURN h.name AS Name, TagCount
ORDER BY TagCount DESC"

# Count Users Tagged ----------------------------------------------------------
qsCountUsersTagged <- "
MATCH (u1:User:Tweeted)-[:POSTS]->(t1:Tweet)<-[:TAGS]-(h:Hashtag)
WHERE h.name =~ {tag}
RETURN COUNT(DISTINCT u1)"

# Users Tagged ----------------------------------------------------------
qsUsersTagged <- "
MATCH (u1:User:Tweeted)-[:POSTS]->(t1:Tweet)<-[:TAGS]-(h:Hashtag)
WHERE h.name =~ {tag}
RETURN u1.screen_name AS screen_name, h.name AS tag"

# Collect Top Tags ----------------------------------------------------------
qsCollectTopTags <- "
MATCH (l:List)<-[:IN_LIST]-(cm:Community)<-[:MEMBER_OF_COMMUNITY]-(u1:User)
WITH cm,u1,l
MATCH (u1)-[:POSTS]->(t1:Tweet)<-[TAGS]-(h:Hashtag)
WITH  cm,l,h, COUNT(h) AS Count
WHERE Count >=50
RETURN COLLECT(h.name)"

# Tags By List Type ----------------------------------------------------------
qsTagsByListType <- "
MATCH (l:List)<-[:IN_LIST]-(cm:Community)<-[:MEMBER_OF_COMMUNITY]-(u1:User)
WITH cm,u1,l
MATCH (u1)-[:POSTS]->(t1:Tweet)<-[TAGS]-(h:Hashtag)
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
WITH  cm,l,h, COUNT(h) AS Count
WHERE Count >={min_count}
RETURN DISTINCT l.type AS Community, h.name AS Name, SUM(Count) AS TagCount 
ORDER BY l.type, TagCount DESC"


# Check AlphaText ------------------------------------------------------
qsAlphaTextCheck <- "MATCH (t1:Tweet) WHERE EXISTS(t1.alpha_text) RETURN COUNT(t1)"

# Words Overall ------------------------------------------------------
qsOverallWordCount <- "MATCH (u:User)-[:POSTS]->(t:Tweet) 
WHERE u.screen_name IN {vList}
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
AND  EXISTS(t.alpha_text)  
RETURN t.alpha_text"

# Words by Community ------------------------------------------------------
qsWordsByCommunity <- "MATCH (cm1:Community)<-[:MEMBER_OF_COMMUNITY]-(u1:User)-[:POSTS]->(t1:Tweet)
WHERE EXISTS(t1.alpha_text) AND cm1.name = {community}
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
AND u1.screen_name IN ({usersForDetection})
RETURN  t1.alpha_text AS alpha_text"

# Words by Community ------------------------------------------------------
qsWordsByCommunityInternal <- "MATCH (cm1:Community)<-[:MEMBER_OF_COMMUNITY]-(u1:User)-[:POSTS]->(t1:Tweet)-[:MENTIONS|:REPLY_TO*1..2]->(u2:User)-[:MEMBER_OF_COMMUNITY]->(cm2:Community)
WHERE EXISTS(t1.alpha_text) AND cm1.name = {community} AND cm2.name = {community}
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
AND cm1.name = cm2.name
AND u1.screen_name <> u2.screen_name
AND u1.screen_name IN ({usersForDetection})
RETURN  t1.alpha_text AS alpha_text
UNION
MATCH (cm3:Community)<-[:MEMBER_OF_COMMUNITY]-(u3:User)-[:POSTS]->(t2:Tweet)<-[:RETWEETS]-(t3:Tweet)<-[:POSTS]-(u4:User)-[:MEMBER_OF_COMMUNITY]->(cm4:Community)
WHERE EXISTS(t2.alpha_text) AND cm3.name = {community}
AND t2.unixtime >= TOINT({startTimeUnix}) AND t2.unixtime <= TOINT({endTimeUnix})
AND cm3.name = cm4.name
AND u3.screen_name <> u4.screen_name
AND u4.screen_name IN ({usersForDetection})
RETURN  t2.alpha_text AS alpha_text"

# Tweet Corpus by Community ------------------------------------------------------
qsTweetCorpusByCommunity <- "MATCH (cm1:Community)<-[:MEMBER_OF_COMMUNITY]-(u1:User)-[:POSTS]->(t1:Tweet)
WHERE EXISTS(t1.alpha_text) AND cm1.name = {community}
AND cm1.subgraph = {subgraph}
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
RETURN  t1.alpha_text AS alpha_text"

# Tweet Corpus by Community Internal ------------------------------------------------------
qsTweetCorpusByCommunityInternal <- "MATCH (cm1:Community)<-[:MEMBER_OF_COMMUNITY]-(u1:User)-[:POSTS]->(t1:Tweet)-[:MENTIONS|:REPLY_TO*1..2]->(u2:User)-[:MEMBER_OF_COMMUNITY]->(cm2:Community)
WHERE EXISTS(t1.alpha_text) AND cm1.name = {community}
AND cm1.subgraph = {subgraph}
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
AND cm1.name = cm2.name
AND u1.screen_name <> u2.screen_name
RETURN  t1.alpha_text AS alpha_text
UNION
MATCH (cm3:Community)<-[:MEMBER_OF_COMMUNITY]-(u3:User)-[:POSTS]->(t2:Tweet)<-[:RETWEETS]-(t3:Tweet)<-[:POSTS]-(u4:User)-[:MEMBER_OF_COMMUNITY]->(cm4:Community)
WHERE EXISTS(t2.alpha_text) AND cm3.name = {community} 
AND cm3.subgraph = {subgraph}
AND t2.unixtime >= TOINT({startTimeUnix}) AND t2.unixtime <= TOINT({endTimeUnix})
AND cm3.name = cm4.name
AND u3.screen_name <> u4.screen_name
RETURN  t2.alpha_text AS alpha_text"

# Tweet Corpus by Flock Internal  ------------------------------------------------------
qsTweetCorpusByFlockInternal <- "MATCH (cm1:Community)<-[m:MEMBER_OF_COMMUNITY]-(u1:User)-[:POSTS]->(t1:Tweet)-[:MENTIONS|:REPLY_TO*1..2]->(u2:User)-[:MEMBER_OF_COMMUNITY]->(cm2:Community)
WHERE EXISTS(t1.alpha_text) AND cm1.name = {community}
AND cm1.type = 'flock'
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
AND cm1.name = cm2.name
AND u1.screen_name <> u2.screen_name
AND m.active = TRUE
AND u1.screen_name IN ({usersForDetection})
RETURN  t1.alpha_text AS alpha_text
UNION
MATCH (cm3:Community)<-[m1:MEMBER_OF_COMMUNITY]-(u3:User)-[:POSTS]->(t2:Tweet)<-[:RETWEETS]-(t3:Tweet)<-[:POSTS]-(u4:User)-[:MEMBER_OF_COMMUNITY]->(cm4:Community)
WHERE EXISTS(t2.alpha_text) AND cm3.name = {community}
AND cm3.type = 'flock'
AND t2.unixtime >= TOINT({startTimeUnix}) AND t2.unixtime <= TOINT({endTimeUnix})
AND cm3.name = cm4.name
AND u3.screen_name <> u4.screen_name
AND m1.active = TRUE
AND u4.screen_name IN ({usersForDetection})
RETURN  t2.alpha_text AS alpha_text"

# Tweet Corpus by Flock ------------------------------------------------------
qsTweetCorpusByFlock <- "MATCH (cm1:Community)<-[m:MEMBER_OF_COMMUNITY]-(u1:User)-[:POSTS]->(t1:Tweet)
WHERE EXISTS(t1.alpha_text) AND cm1.name = {community}
AND cm1.type = 'flock'
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
AND m.active = TRUE
AND u1.screen_name IN ({usersForDetection})
RETURN  t1.alpha_text AS alpha_text"

# Tags by Community ------------------------------------------------------
qsTagsByCommunity <- "MATCH (cm1:Community)<-[:MEMBER_OF_COMMUNITY]-(u1:User)-[:POSTS]->(t1:Tweet)
WHERE cm1.name = {community}
AND t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
AND u1.screen_name IN ({usersForDetection})
WITH t1,u1, cm1 AS cm
MATCH (t1)<-[:TAGS]-(h:Hashtag)
RETURN cm.name AS Community,u1.screen_name AS User,h.name AS Name"

# Tags by Community ------------------------------------------------------
qsTagsByCommunityInternal <- "MATCH (cm1:Community)<-[:MEMBER_OF_COMMUNITY]-(u1:User)-[:POSTS]->(t1:Tweet)-[:MENTIONS|:REPLY_TO*1..2]->(u2:User)-[:MEMBER_OF_COMMUNITY]->(cm2:Community)
WHERE cm1.name = {community}
AND cm1.name = cm2.name
AND u1.screen_name <> u2.screen_name
AND u1.screen_name IN ({usersForDetection})
WITH  t1,u1, cm1 AS cm
MATCH (t1)<-[:TAGS]-(h:Hashtag)
WHERE t1.unixtime >= TOINT({startTimeUnix}) AND t1.unixtime <= TOINT({endTimeUnix})
RETURN cm.name AS Community,u1.screen_name AS User,h.name AS Name
UNION
MATCH (cm3:Community)<-[:MEMBER_OF_COMMUNITY]-(u3:User)-[:POSTS]->(t2:Tweet)<-[:RETWEETS]-(t3:Tweet)<-[:POSTS]-(u4:User)-[:MEMBER_OF_COMMUNITY]->(cm4:Community)
WHERE cm3.name = {community} 
AND cm3.name = cm4.name
AND u3.screen_name <> u4.screen_name
AND u4.screen_name IN ({usersForDetection})
WITH  t2,u3, cm3 AS cm
MATCH (t2)<-[:TAGS]-(h:Hashtag)
WHERE t2.unixtime >= TOINT({startTimeUnix}) AND t2.unixtime <= TOINT({endTimeUnix})
RETURN cm.name AS Community,u3.screen_name AS User,h.name AS Name"

# Get Black White ----------------------------------------------------------
qsGetBlackWhite <- "MATCH (u:User)-[r:MEMBER_OF_COMMUNITY]->(cm:Community)-[IN_LIST]->(l:List)
WHERE l.type = 'Black List'
WITH u,cm
MATCH (u)-[r2:MEMBER_OF_COMMUNITY]->(cm2:Community)-[IN_LIST]->(l2:List)
WHERE l2.type = 'White List'
RETURN u.name, COLLECT(cm.name),COLLECT(cm2.name)
LIMIT 100"

# Get Black ----------------------------------------------------------
qsGetBlack <- "MATCH (u:User)-[:MEMBER_OF_COMMUNITY]->(cm:Community)-[:IN_LIST]->(l:List)
WHERE l.type = 'Black List'
RETURN DISTINCT u.screen_name AS name, u.verified AS Verified, 
u.followers as Followers, u.name AS FullName"

# Get Community Users ----------------------------------------------------------
qsGetCommunityUsers <- "MATCH (u:User)-[:MEMBER_OF_COMMUNITY]->(cm:Community)
WHERE cm.name = {community_name}
RETURN DISTINCT u.screen_name AS name, u.verified AS Verified, 
u.followers as Followers, u.name AS FullName"

# Auto Tag Verified ----------------------------------------------------------
qsAutoTagVerified <- "MATCH (cm:Community {name:'Twitter Verified'})
WITH cm
MATCH (u:User) WHERE  u.verified = TRUE
WITH cm, u
MATCH (u) WHERE NOT (u)-[:MEMBER_OF_COMMUNITY]->(cm)
WITH u,cm
MERGE (u)-[r:MEMBER_OF_COMMUNITY {status: 'auto'}]->(cm)"

# Get Tags By Topic ----------------------------------------------------------
qsGetTagsByTopic <- "MATCH (h:Hashtag)-[TAGS]->(t:Tweet)-[ABOUT_TOPIC]->(tp:Topic )
WHERE tp.name = 'OCT14 Dallas Outbreak'
AND t.id > 522899791635034112
AND h.name =~ '.*clip.*'
RETURN h, COUNT(t) AS Count
ORDER BY Count DESC"

# Topic Tag ----------------------------------------------------------
qsTopicTag <- "MATCH (t:Tweet)
WHERE t.text =~ '(.*[tT]homas [dD]uncan.*)|(.*[tT]homas [eE]ric [dD]uncan.*)|(.*[tT]homas [Ee]ric [dD]uncan.*)|(.*[tT]homas [dD]uncan.*)|(.*[dD]uncan\'s.*)'
WITH t
MATCH (tp:Topic {name:'Thomas Duncan'})
MERGE (t)-[r:ABOUT_TOPIC]->(tp)"

# Topic Tag Return ----------------------------------------------------------
qsTopicTagReturn <- "
MATCH (tp1:Topic)
WITH tp1
MATCH (t:Tweet)
WHERE t.text =~ tp1.qstring
WITH t,tp1
MATCH (tp2:Topic {name:tp1.name})
RETURN t, tp2
LIMIT 2"

# Counts Nodes, Tags, Retweets etc ---------------------------------------------

# Count Nodes All ----------------------------------------------------------
qsCountNodesAll <- "MATCH (n) RETURN COUNT(n)"

# Count Re Tweets All ----------------------------------------------------------
qsCountReTweetsAll <- "MATCH (t1:Tweet)<-[r:RETWEETS]-(t2:Tweet)
RETURN COUNT(r)"

# ReTweets ----------------------------------------------------------
qsReTweets <- "MATCH (u1:User)-[p1:POSTS]-(t1:Tweet)
WHERE u1.screen_name = '{}'
WITH u1, COUNT(t1) AS CountP
OPTIONAL MATCH (u1)-[p2:POSTS]-(t2:Tweet)<-[r:RETWEETS]-(t3:Tweet) 
WITH u1, CountP, COUNT(t3) as CountROf
OPTIONAL MATCH (u1)-[p3:POSTS]->(t5:Tweet)-[r:RETWEETS]->(t6:Tweet)
RETURN u1, CountROf, CountP"

# Reach ----------------------------------------------------------
qsReach <- "MATCH (u1:User)-[p1:POSTS *]-(t1:Tweet)<-[r1:RETWEETS *]-(t2:Tweet)<-[p2:POSTS]-(u2:User)
WHERE u1.screen_name IN {listString}
RETURN u1.screen_name, SUM(u2.followers) as Reach" 

# Dates In Graph ----------------------------------------------------------
qsDatesInGraph <- "MATCH (t1:Tweet)
WITH   LEFT(RIGHT(t1.created_at,26),6) AS MonthDay
RETURN DISTINCT MonthDay
LIMIT 100"

# Count With Profile ----------------------------------------------------------
qsCountWithProfile <- "MATCH (u1:User)
WITH COUNT(u1) AS TotalAll
MATCH (u2:User)
WHERE HAS (u2.followers)
WITH TotalAll, COUNT(u2) AS TotalWithFollowers
MATCH (u3:User)
WHERE HAS (u3.profile_complete)
RETURN TotalAll, TotalWithFollowers, COUNT(u3) AS TotalWithProfile"

# Find In Complete Users ----------------------------------------------------------
qsFindInCompleteUsers <- "MATCH (u1:User)
WHERE (NOT u1:FlagScreenName) AND ((NOT EXISTS(u1.profile_complete)) OR
u1.profile_complete = false) 
WITH u1
ORDER BY u1.followers DESC
RETURN DISTINCT u1.screen_name, u1.followers
LIMIT 1000"

# Find Tag ScreenName ----------------------------------------------------------
qsFindTagScreenName <- "
MATCH (u1:User:FlagScreenName)
RETURN DISTINCT u1.screen_name, u1.followers, u1.id
ORDER BY u1.followers DESC
LIMIT 1000"

# Copy Post ----------------------------------------------------------
qsCopyPost <- "
MATCH (u1:User {screen_name:'SauberF1Team'})-[r:POSTS]-(t:Tweet)
,(u2:User {screen_name:'@SauberF1Team'})
MERGE (u2)-[p:POSTS]-(t)
RETURN u1,u2,r,t"

# Delete Node ----------------------------------------------------------
qsDeleteNode <- "
MATCH (n:User { screen_name: {screenName} })-[r]-()
DELETE n,r"

# Mentions Graph ----------------------------------------------------------
qsMentionsGraph <- "
MATCH (u1:User)-[p1:POSTS]->(t1:Tweet)-[m:MENTIONS]->(u2:User)
WHERE ID(u1) <> ID(u2)
RETURN u1.screen_name, u2.screen_name, COUNT(*)
AS weight
ORDER BY weight DESC
LIMIT 100"

# Mentions Retweet Graph ----------------------------------------------------------
qsMentionsRetweetsGraph <- "
MATCH (u1:User)-[p1:POSTS]->(t1:Tweet)-[m:MENTIONS|RETWEETS*..2]->(u2:User)
WHERE ID(u1) <> ID(u2)
RETURN u1.screen_name, u2.screen_name, COUNT(*)
AS weight
ORDER BY weight DESC
LIMIT 100"

# List Duplicates -------------------------------------------------------------------
qsListDuplicates <- "
MATCH (u:User)
WITH u
ORDER BY id(u) DESC // Order by descending to delete the most recent duplicated record
WITH u.id  as DuplicateKey, COUNT(u) as ColCount, COLLECT(ID(u)) as ColNode
WITH DuplicateKey, ColCount, ColNode, HEAD(ColNode) as DuplicateId
WHERE ColCount > 1 AND (DuplicateKey is not null) AND (DuplicateId is not
null)
WITH DuplicateKey, ColCount, ColNode, DuplicateId
ORDER BY DuplicateId
//RETURN DuplicateKey, ColCount, DuplicateId  Toggle comment on/off for validating duplicate records before moving to next step (do not proceed to delete without validating)
RETURN COLLECT(DuplicateId) as CommaSeparatedListOfIds"

# Duplicate Detection ----------------------------------------------------------
qsDuplicateDetection <- "
MATCH (l:Link)
WITH l
ORDER BY id(l) DESC // Order by descending to delete the most recent duplicated record
WITH l.url  as DuplicateKey, COUNT(l) as ColCount, COLLECT(ID(l)) as ColNode
WITH DuplicateKey, ColCount, ColNode, HEAD(ColNode) as DuplicateId
WHERE ColCount > 1 AND (DuplicateKey is not null) AND (DuplicateId is not
null)
WITH DuplicateKey, ColCount, ColNode, DuplicateId
ORDER BY DuplicateId
RETURN DuplicateKey, ColCount, DuplicateId // Toggle comment on/off for validating duplicate records before moving to next step (do not proceed to delete without validating)
//RETURN COLLECT(DuplicateId) as CommaSeparatedListOfIds"

# Duplicate Deletion ----------------------------------------------------------
qsDuplicateDeletion <- "
MATCH (n)-[r]-() // Delete relationships for each duplicate before deleting the node
WHERE ID(n) IN [3396039, 3396863, 3397145, 3397255, 3397716, 3397719, 3397892, 3398745, 3399622, 3399652, 3401264, 3403523, 3405121, 3405483, 3405749, 3405931, 3409878, 3410159, 3410617, 3411138, 3411707, 3413650, 3415785, 3417539, 3418120, 3418364, 3418771, 3418772, 3418813, 3419063, 3419327, 3419556, 3421061, 3444812, 3444855, 3444991, 3446151, 3469040, 3469202, 3469343, 3469346, 3469362, 3469481, 3469943, 3470121, 3470145, 3494010, 3494205, 3518950, 3519192, 3519195, 3519220, 3519474, 3519544, 3519600, 3519740, 3519756, 3519913, 3520302, 3520374, 3520385, 3520535, 3520615, 3520796, 3520917, 3520996, 3521150, 3521155, 3521164, 3521176, 3521352, 3543911, 3543931, 3543935, 3543946, 3543949, 3544129, 3544161, 3544173, 3544300, 3544309, 3544317, 3544412, 3544431, 3544436, 3544613, 3544671, 3544696, 3544759, 3544773, 3544778, 3545163, 3545172, 3545179, 3545369, 3545455, 3545556, 3545637, 3545791, 3545802, 3545875, 3545991, 3546097, 3546217, 3546227, 3546278, 3546671]
// Replace highlighted ids with CommaSeparatedListOfIds from step 1
DELETE r, n"

# Users Tweets Between Ids ----------------------------------------------------------
qsUsersTweetsBetweenIds <- "MATCH (u1:User {screen_name:'cnnbrk'})-[POSTS]->(t1:Tweet)
WHERE
t1.id >= TOINT('517572484355592192') AND t1.id <= TOINT('520000496355246080') 
RETURN u1, t1"