# Initialise Environment ------------------------------------
print(paste0(Sys.time()," - Initialising"))
rm(list = ls())
source('~/Config.R')
library("ProjectTemplate", lib.loc = libraryLoc)
setwd(workingDir)
load.project()

graph <- GraphInitialise(graphConnectionString = neo4jString)
scheduleList <- GetRunSettings(dbHost = dbHost,dbUser = dbUser,dbPwd = dbPwd,
                               dbName = dbName, sampleMode = sampleMode)

if (length(scheduleList) == 0) {
      stop("No Scheduled Runs")}

for (i in 1:scheduleList$scheduleItems) {
      
      startDate <- scheduleList$startDates[i]
      endDate <- scheduleList$endDates[i]
      scheduleId <- scheduleList$scheduleId[i]
      uniqueName <- scheduleList$uniqueName[i]
      useMentions <- scheduleList$useMentions[i]
      startTimeUnix <- scheduleList$startTimeUnix[i]
      endTimeUnix <- scheduleList$endTimeUnix[i]
      
      
      # Create Network Name ----------------------------------------------
      networkNames <- GetNetworkNames(startDate = startDate, endDate = endDate, scheduleId = scheduleId, searchName = searchName, outputDir = outputDir, uniqueName = uniqueName, useMentions = useMentions)
      networkName <- networkNames[[1]]
      origName <- networkNames[[2]]
      filePath <- networkNames[[3]]
      fileName <- networkNames[[4]]
      origFileName <- networkNames[[5]]
      print(origName)
      print(paste0(filePath," - Started PageRank"))
      print(paste0(fileName," - Started PageRank"))
      
      # Direct Output to file
      sink(paste0("logs/", networkName, ".log"))
      print(paste0(Sys.time()," - Starting Run Schedule: ", scheduleList$startDates[1]))
      
      if (scheduleList$processGraph[i] == TRUE) {
            if (scheduleList$doDetectionList[i]) {
                  
                  # Run Analysis ----------------------------------------
                  print(paste0(Sys.time()," - Starting - Generate Graph"))
                  g <- GenerateNetwork1(graph,useMentions = scheduleList$useMentionsList[i],limitHours = limitHours,
                                        startTimeUnix = scheduleList$startTimeUnix[i], endTimeUnix = scheduleList$endTimeUnix[i])
                  if (is.null(g)) { stop("No Graph in Period") }
                  
                  PrintGraphStats(g,startTimeUnix = startTimeUnix, endTimeUnix = endTimeUnix)
                  
                  # spews out igraph pagerank to a csv file
                  PageRankIgraph(g)
                  # spews out igraph pagerank to a csv file
                  BetweennessIgraph(g)

                  
            }else{
                  print(paste0(Sys.time()," - Starting - Load File"))
                  load(origFileName)
                  # Reset networkName
                  if (scheduleList$uniqueName[i] == TRUE) {
                        networkName <- paste0(networkName,"_",scheduleList$scheduleId[i])
                  }
                  print(paste0(Sys.time()," - Completed - Load File"))
            }
            
      }else{
            # OR load existing.
            load(origFileName)
            # Reset networkName
            if (uniqueName == TRUE) {
                  networkName <- paste0(networkName,"_",scheduleList$scheduleId[i])
            }
      }
      
      
      # Set Schedule Complete in MySQL
      conn <- dbConnect(MySQL(), user = dbUser, password = dbPwd, host = dbHost, dbname = dbName)
      sqlUpdate <- paste0("UPDATE run_schedule SET completed = 1 WHERE id = ",scheduleId,";" )
      res <- dbSendQuery(conn, sqlUpdate)
      dbClearResult(res)
      dbDisconnect(conn)
      print(paste0(Sys.time()," - Completed - Period"))
      print(paste0("---------------------------------------------------------------"))
      
      # Redirect Output to Console
      sink()
}


